.PHONY: ManUtils indexer clean

all: ManUtils indexer


ManUtils: ManUtils/blib/lib/ManUtils.pm

ManUtils/blib/lib/ManUtils.pm: ManUtils/Makefile.PL ManUtils/ManUtils.pm ManUtils/ManUtils.xs web/target/release/libweb.a
	-test ManUtils/ManUtils.xs -ot web/target/release/libweb.a && touch -r web/target/release/libweb.a ManUtils/ManUtils.xs
	cd ManUtils && perl Makefile.PL && make
	touch ManUtils/blib/lib/ManUtils.pm

web/target/release/libweb.a: web/Cargo.toml web/src/*.rs
	cd web && cargo build --release
	#strip --strip-unneeded web/target/release/libweb.a

indexer: indexer/target/release/indexer

indexer/target/release/indexer: indexer/Cargo.toml indexer/src/*.rs
	cd indexer && cargo build --release

clean:
	make -C ManUtils clean
	rm -f ManUtils/Makefile.old
	cd indexer && cargo clean
	cd web && cargo clean
