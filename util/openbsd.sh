#!/bin/sh

. ./common.sh

AMIRROR=http://ftp.eu.openbsd.org/pub/OpenBSD/
CMIRROR=http://cdn.openbsd.org/pub/OpenBSD/

index_base() {
    local ver=$1
    local date=$2
    local mirror=${3:-$CMIRROR}
    for pkg in base comp game man xbase xserv xshare; do
        index pkg --sys openbsd-$ver --pkg $pkg --ver $ver --date $date "${mirror}$ver/amd64/$pkg${ver/./}.tgz"
    done
}

case $1 in
    5.0)
        index_base 5.0 2011-11-01 $AMIRROR
        ;;

    5.1)
        index_base 5.1 2012-05-01 $AMIRROR
        ;;

    5.2)
        index_base 5.2 2012-11-01 $AMIRROR
        ;;

    5.3)
        index_base 5.3 2013-05-01 $AMIRROR
        ;;

    5.4)
        index_base 5.4 2013-11-01 $AMIRROR
        ;;

    5.5)
        index_base 5.5 2014-05-01 $AMIRROR
        ;;

    5.6)
        index_base 5.6 2014-11-01 $AMIRROR
        ;;

    5.7)
        index_base 5.7 2015-05-01 $AMIRROR
        ;;

    5.8)
        index_base 5.8 2015-10-18 $AMIRROR
        ;;

    5.9)
        index_base 5.9 2016-05-29 $AMIRROR
        ;;

    6.0)
        index_base 6.0 2016-09-01 $AMIRROR
        ;;

    6.1)
        index_base 6.1 2017-04-11 $AMIRROR
        ;;

    6.2)
        index_base 6.2 2017-10-09 $AMIRROR
        ;;

    6.3)
        index_base 6.3 2018-04-02 $AMIRROR
        ;;

    6.4)
        index_base 6.4 2018-10-18 $AMIRROR
        ;;

    6.5)
        index_base 6.5 2019-04-24 $AMIRROR
        ;;

    6.6)
        index_base 6.6 2019-10-17 $AMIRROR
        ;;

    6.7)
        index_base 6.7 2020-05-19 $AMIRROR
        ;;

    6.8)
        index_base 6.8 2020-10-18 $AMIRROR
        ;;

    6.9)
        index_base 6.9 2021-05-01 $AMIRROR
        ;;

    7.0)
        index_base 7.0 2021-10-14 $AMIRROR
        ;;

    7.1)
        index_base 7.1 2022-04-21 $AMIRROR
        ;;

    7.2)
        index_base 7.2 2022-10-20 $AMIRROR
        ;;

    7.3)
        index_base 7.3 2023-04-10
        ;;

    7.4)
        index_base 7.4 2023-10-16
        ;;

    7.5)
        index_base 7.5 2024-04-05
        ;;

    7.6)
        index_base 7.6 2024-10-08
        ;;

    old)
        $0 5.0
        $0 5.1
        $0 5.2
        $0 5.3
        $0 5.4
        $0 5.5
        $0 5.6
        $0 5.7
        $0 5.8
        $0 5.9
        $0 6.0
        $0 6.1
        $0 6.2
        $0 6.3
        $0 6.4
        $0 6.5
        $0 6.6
        $0 6.7
        $0 6.8
        $0 6.9
        $0 7.0
        $0 7.1
        $0 7.2
        $0 7.3
        $0 7.4
        $0 7.5
        $0 7.6
        ;;
esac
