#!/bin/sh

. ./common.sh

CMIRROR=http://cdn.netbsd.org/pub/NetBSD/
AMIRROR=http://archive.netbsd.org/pub/NetBSD-archive/

# NetBSD's distribution format has been delightfully stable across the years.
# This directory structure and the exact list of sets has been pretty much the
# same since 1.3.
index_base() {
    local ver=$1
    local date=$2
    local ext=${3:-tar.xz}
    local mirror=${4:-$CMIRROR}
    local arch=${5:-amd64}
    for pkg in comp games man text xbase xcomp xserver; do
        index pkg --sys netbsd-$ver --pkg $pkg --ver $ver --date $date "${mirror}NetBSD-$ver/$arch/binary/sets/$pkg.$ext"
    done
}

case $1 in
    1.3)
        index_base 1.3 1998-01-03 tgz $AMIRROR i386
        ;;

    1.3.2)
        index_base 1.3.2 1998-05-29 tgz $AMIRROR i386
        ;;

    1.3.3)
        index_base 1.3.3 1998-12-23 tgz $AMIRROR i386
        ;;

    1.4)
        index_base 1.4 1999-05-12 tgz $AMIRROR i386
        ;;

    1.4.1)
        index_base 1.4.1 1999-08-26 tgz $AMIRROR i386
        ;;

    1.4.2)
        index_base 1.4.2 2000-03-21 tgz $AMIRROR i386
        ;;

    1.4.3)
        index_base 1.4.3 2000-11-25 tgz $AMIRROR i386
        ;;

    1.5)
        index_base 1.5 2000-12-06 tgz $AMIRROR i386
        ;;

    1.5.1)
        index_base 1.5.1 2001-07-11 tgz $AMIRROR i386
        ;;

    1.5.2)
        index_base 1.5.2 2001-09-13 tgz $AMIRROR i386
        ;;

    1.5.3)
        index_base 1.5.3 2002-07-22 tgz $AMIRROR i386
        ;;

    1.6)
        index_base 1.6 2002-09-14 tgz $AMIRROR i386
        ;;

    1.6.1)
        index_base 1.6.1 2003-04-21 tgz $AMIRROR i386
        ;;

    1.6.2)
        index_base 1.6.2 2004-03-01 tgz $AMIRROR i386
        ;;

    2.0)
        index_base 2.0 2004-12-09 tgz $AMIRROR i386
        ;;

    2.0.2)
        index_base 2.0.2 2005-04-14 tgz $AMIRROR i386
        ;;

    # Archive has no binary files for this release :(
    #2.0.3)
    #    index_base 2.0.3 2005-10-31 tgz $AMIRROR i386
    #    ;;

    2.1)
        index_base 2.1 2005-11-02 tgz $AMIRROR i386
        ;;

    3.0)
        index_base 3.0 2005-12-23 tgz $AMIRROR i386
        ;;

    3.0.1)
        index_base 3.0.1 2006-07-24 tgz $AMIRROR i386
        ;;

    3.0.2)
        index_base 3.0.2 2006-11-04 tgz $AMIRROR i386
        ;;

    3.1)
        index_base 3.1 2006-11-04 tgz $AMIRROR i386
        ;;

    4.0)
        index_base 4.0 2007-12-19 tgz $AMIRROR i386
        ;;

    4.0.1)
        index_base 4.0.1 2008-10-14 tgz $AMIRROR i386
        ;;

    5.0)
        index_base 5.0 2009-04-29 tgz $AMIRROR i386
        ;;

    5.0.1)
        index_base 5.0.1 2009-08-02 tgz $AMIRROR i386
        ;;

    5.0.2)
        index_base 5.0.2 2010-02-12 tgz $AMIRROR i386
        ;;

    5.1)
        index_base 5.1 2010-11-19 tgz $AMIRROR i386
        ;;

    5.1.2)
        index_base 5.1.2 2012-02-02 tgz $AMIRROR i386
        ;;

    5.1.3)
        index_base 5.1.3 2012-09-28 tgz $AMIRROR i386
        ;;

    5.1.4)
        index_base 5.1.4 2014-01-27 tgz $AMIRROR i386
        ;;

    5.1.5)
        index_base 5.1.5 2014-11-15 tgz $AMIRROR i386
        ;;

    5.2)
        index_base 5.2 2012-12-03 tgz $AMIRROR i386
        ;;

    5.2.1)
        index_base 5.2.1 2012-09-28 tgz $AMIRROR i386
        ;;

    5.2.2)
        index_base 5.2.2 2014-01-27 tgz $AMIRROR i386
        ;;

    5.2.3)
        index_base 5.2.3 2014-11-15 tgz $AMIRROR i386
        ;;

    6.0)
        index_base 6.0 2012-10-17 tgz $AMIRROR
        ;;

    6.0.1)
        index_base 6.0.1 2012-12-26 tgz $AMIRROR
        ;;

    6.0.2)
        index_base 6.0.2 2013-05-18 tgz $AMIRROR
        ;;

    6.0.3)
        index_base 6.0.3 2013-09-30 tgz $AMIRROR
        ;;

    6.0.4)
        index_base 6.0.4 2014-01-27 tgz $AMIRROR
        ;;

    6.0.5)
        index_base 6.0.5 2014-04-12 tgz $AMIRROR
        ;;

    6.0.6)
        index_base 6.0.6 2014-09-22 tgz $AMIRROR
        ;;

    6.1)
        index_base 6.1 2013-05-18 tgz $AMIRROR
        ;;

    6.1.1)
        index_base 6.1.1 2013-08-15 tgz $AMIRROR
        ;;

    6.1.2)
        index_base 6.1.2 2013-09-26 tgz $AMIRROR
        ;;

    6.1.3)
        index_base 6.1.3 2014-01-18 tgz $AMIRROR
        ;;

    6.1.4)
        index_base 6.1.4 2014-04-12 tgz $AMIRROR
        ;;

    6.1.5)
        index_base 6.1.5 2014-09-22 tgz $AMIRROR
        ;;

    7.0)
        index_base 7.0 2015-09-25 tgz $AMIRROR
        ;;

    7.0.1)
        index_base 7.0.1 2016-05-22 tgz $AMIRROR
        ;;

    7.0.2)
        index_base 7.0.2 2016-10-21 tgz $AMIRROR
        ;;

    7.1)
        index_base 7.1 2017-03-11 tgz $AMIRROR
        ;;

    7.1.1)
        index_base 7.1.1 2017-12-22 tgz $AMIRROR
        ;;

    7.1.2)
        index_base 7.1.2 2018-03-15 tgz $AMIRROR
        ;;

    7.2)
        index_base 7.2 2018-08-29 tgz $AMIRROR
        ;;

    8.0)
        index_base 8.0 2018-07-17 tgz $AMIRROR
        ;;

    8.1)
        index_base 8.1 2019-05-31 tgz $AMIRROR
        ;;

    8.2)
        index_base 8.2 2020-03-31 tgz $AMIRROR
        ;;

    8.3)
        index_base 8.3 2024-05-04 tgz $AMIRROR
        ;;

    9.0)
        index_base 9.0 2020-02-14
        ;;

    9.1)
        index_base 9.1 2020-10-18
        ;;

    9.2)
        index_base 9.2 2021-05-12
        ;;

    9.3)
        index_base 9.3 2022-08-04
        ;;

    9.4)
        index_base 9.4 2024-04-20
        ;;

    10.0)
        index_base 10.0 2024-03-28
        ;;

    10.1)
        index_base 10.1 2024-12-16
        ;;

    old)
        $0 1.3
        $0 1.3.2
        $0 1.3.3
        $0 1.4
        $0 1.4.1
        $0 1.4.2
        $0 1.4.3
        $0 1.5
        $0 1.5.1
        $0 1.5.2
        $0 1.5.3
        $0 1.6
        $0 1.6.1
        $0 1.6.2
        $0 2.0
        $0 2.0.2
        #$0 2.0.3
        $0 2.1
        $0 3.0
        $0 3.0.1
        $0 3.0.2
        $0 3.1
        $0 4.0
        $0 4.0.1
        $0 5.0
        $0 5.0.1
        $0 5.0.2
        $0 5.1
        $0 5.1.2
        $0 5.1.3
        $0 5.1.4
        $0 5.1.5
        $0 5.2
        $0 5.2.1
        $0 5.2.2
        $0 5.2.3
        $0 6.0
        $0 6.0.1
        $0 6.0.2
        $0 6.0.3
        $0 6.0.4
        $0 6.0.5
        $0 6.0.6
        $0 6.1
        $0 6.1.1
        $0 6.1.2
        $0 6.1.3
        $0 6.1.4
        $0 6.1.5
        $0 7.0
        $0 7.0.1
        $0 7.0.2
        $0 7.1
        $0 7.1.1
        $0 7.1.2
        $0 7.2
        $0 8.0
        $0 8.1
        $0 8.2
        $0 8.3
        $0 9.0
        $0 9.1
        $0 9.2
        $0 9.3
        $0 9.4
        $0 10.0
        $0 10.1
        ;;
esac
