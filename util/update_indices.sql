-- Create a new table before replacing in order to avoid a long-held lock on
-- the table being replaced. The site should remain responsive while these
-- queries are run.
BEGIN;
CREATE TABLE stats_cache_new AS
    SELECT (SELECT count(*) FROM contents) AS hashes,
           (SELECT count(distinct name) FROM mans) AS mans, *
      FROM (SELECT count(*), count(distinct pkgver) FROM files) x(files, packages);
DROP TABLE stats_cache;
ALTER TABLE stats_cache_new RENAME TO stats_cache;
COMMIT;


-- Update c_hasman.
-- This query is commented out because the indexer will take care to set the
-- c_hasman column automatically. It's included here as "documentation" so it
-- can be run manually when package versions or man pages are removed from the
-- database.
--
-- UPDATE packages SET c_hasman = NOT c_hasman
--  WHERE c_hasman <> EXISTS(SELECT 1 FROM package_versions pv WHERE pv.package = packages.id AND EXISTS(SELECT 1 FROM files f WHERE f.pkgver = pv.id));
